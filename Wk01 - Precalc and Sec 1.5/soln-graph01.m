f = @(t) (-4 <= t && t <= -1) .* (2 + .5 .* (t+3));
g = @(t) -1 + 4.* (t-1).^2;
x1 = -4:.05:-1.05;
x1 = -4:.05:.95;
x2 = 1.05:.05:3;
plot(x1,f(x1),'k','linewidth',4)
f = @(t) (-4 <= t && t <= 1) .* (2 + .5 .* (t+3));
plot(x1,f(x1),'k','linewidth',4)
hold on
plot(x2,g(x2),'k','linewidth',4)
hold on
scatter([-3 1],[4 -1],100,'r','filled')
hold on
scatter([-3 1],[f(-3),f(1)],100,'b')
axis('tic')
yticks([-1 2 4])