

# the function
f = @(t) (t.^2-t-2)./(t.^2-1);
%g = @(t) t - 3^(-.5) + 3^(-1.5);
%h = @(t) t + 3^(-.5) - 3^(-1.5);
%m = (f(1.003)-f(1))/.003;
%g = @(t) m.*(t-1) + f(1);

epsilon = .05
v1 = -3:.05:1-epsilon;
v3 = 1+epsilon:.05:3;
%vx2 = 2.1:1:10;
%vx2 = -5:.1:5;
%dot = [1,3];







plot(v1, f(v1), 'k', 'linewidth', 5);
hold on
plot(v3, f(v3), 'k', 'linewidth', 5);
hold on
%plot([-5,5],[0,0],'k','linewidth',1);
%hold on
%plot([0,0],[-50,50],'k','linewidth',1);
%hold on
%plot(vx, g(vx), 'b', 'linewidth', 3);
%hold on
%plot(vx, h(vx), 'b', 'linewidth', 3);
%hold on
%scatter(dot,f(dot),100,'r','filled');
%hold on
%plot([-5 5], [0, 0], 'k', 'linewidth', 1);
%hold on 
%plot([0, 0], [-5 5],'k', 'linewidth', 1);

# make it a square
%axis ([-2 2 -4.5 4.5]); 
%axis([2 6 -1 7]);

# make axes thick
h=get(gcf, "currentaxes");
set(h, "fontsize", 14, "linewidth", 5);