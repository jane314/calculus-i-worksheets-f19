

# the function
f = @(t) 3+t.^2;
g = @(t) 2-t.^2;



vx = -10:.05:10;
vx2 = -10:.05:10;
s = [0,4];







plot(f(vx), vx, 'k', 'linewidth', 5);
hold on
plot(g(vx2), vx2, 'k', 'linewidth', 5);
hold on
plot([-10 10], [1 1], 'b', 'linewidth', 5);
hold on
plot([-10 10], [2 2], 'b', 'linewidth', 5);
hold on
plot([-40 40],[0,0],'k','linewidth',1);
hold on
%plot([0,0],[-40,40],'k','linewidth',1);
%hold on
%scatter([0,4], [0,f(4)],200,'r','filled');

# make it a square
axis ([-5, 10, -4, 4]); 
%axis([-1.5 1.5 -6 6]);


# make axes thick
h=get(gcf, "currentaxes");
set(h, "fontsize", 14, "linewidth", 2);