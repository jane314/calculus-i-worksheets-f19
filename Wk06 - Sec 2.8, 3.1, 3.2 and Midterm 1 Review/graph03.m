

# the function
f = @(t) 3-abs(t-2);
%m = (f(1.003)-f(1))/.003;
%g = @(t) m.*(t-1) + f(1);


vx = -3:.1:5;
%vx2 = -5:.1:5;
%dot = [1];







plot(vx, f(vx), 'k', 'linewidth', 5);
%hold on
%plot(vx2,g(vx2),'r','linewidth',3);

hold on
scatter([2],[f(2)],100,'r','filled');



# make it a square
%axis ([0, 5, -1, 6]); 
axis([-1 6 0 5]);

# make axes thick
h=get(gcf, "currentaxes");
set(h, "fontsize", 14, "linewidth", 5);