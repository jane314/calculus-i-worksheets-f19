

# the function
f = @(t) 2.*t.^2+10;
g = @(t) 4.*t+16;




vx = -10:.05:10;
vx2 = -10:.05:10;
s = [0,4];







plot(vx, f(vx), 'k', 'linewidth', 5);
hold on
plot(vx2,g(vx2),  'k', 'linewidth', 5);
hold on
plot([5 5], [-100 100], 'b', 'linewidth', 5);
hold on
plot([-2 -2], [-100 100], 'b', 'linewidth', 5);
hold on
plot([-40 40],[0,0],'k','linewidth',1);
hold on
%plot([0,0],[-40,40],'k','linewidth',1);
%hold on
%scatter([0,4], [0,f(4)],200,'r','filled');

# make it a square
axis ([-4, 6, -10, 80]); 
%axis([-1.5 1.5 -6 6]);


# make axes thick
h=get(gcf, "currentaxes");
set(h, "fontsize", 14, "linewidth", 2);