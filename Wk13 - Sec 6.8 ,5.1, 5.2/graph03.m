

# the function
f = @(t) 3-t.^2;
g = @(t) 1;

vx = -10:.05:20;
s = [-2,2];







plot(f(vx), vx, 'k', 'linewidth', 5);
hold on
plot([-1 -1], [-10 10], 'b', 'linewidth', 5);
hold on
plot([-4 4],[0,0],'k','linewidth',1);
hold on
scatter(f(s), s,200,'r','filled');

# make it a square
axis ([-2, 3.5, -3, 3]); 
%axis([-1.5 1.5 -6 6]);


# make axes thick
h=get(gcf, "currentaxes");
set(h, "fontsize", 14, "linewidth", 2);