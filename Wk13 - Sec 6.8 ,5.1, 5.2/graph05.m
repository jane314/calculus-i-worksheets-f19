

# the function
f = @(t) 8./t;
g = @(t) 2.*t;


vx = .1:.05:10;
vx2 = -10:.05:10;
s = [0,4];







plot(vx, f(vx), 'k', 'linewidth', 5);
hold on
plot(vx2, g(vx2), 'm', 'linewidth', 5);
hold on
plot([0 0], [-100 100], 'b', 'linewidth', 5);
hold on
plot([4 4], [-100 100], 'b', 'linewidth', 5);
hold on
plot([-4 40],[0,0],'k','linewidth',1);
hold on
scatter([0,4], [0,f(4)],200,'r','filled');

# make it a square
axis ([-1, 6, -5, 20]); 
%axis([-1.5 1.5 -6 6]);


# make axes thick
h=get(gcf, "currentaxes");
set(h, "fontsize", 14, "linewidth", 2);