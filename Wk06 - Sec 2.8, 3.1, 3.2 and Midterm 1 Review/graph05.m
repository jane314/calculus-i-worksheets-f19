

# the function
f = @(t) t.^3;
g = @(t) t - 3^(-.5) + 3^(-1.5);
h = @(t) t + 3^(-.5) - 3^(-1.5);
%m = (f(1.003)-f(1))/.003;
%g = @(t) m.*(t-1) + f(1);


vx = -9:.1:9;
%vx2 = 2.1:1:10;
%vx2 = -5:.1:5;
dot = [3^(-.5), -3^(-.5)];







plot(vx, f(vx), 'k', 'linewidth', 5);
hold on
plot(vx, g(vx), 'b', 'linewidth', 3);
hold on
plot(vx, h(vx), 'b', 'linewidth', 3);
hold on
scatter(dot,f(dot),80,'r','filled');
hold on
plot([-5 5], [0, 0], 'k', 'linewidth', 1);
hold on 
plot([0, 0], [-5 5],'k', 'linewidth', 1);

# make it a square
%axis ([0, 5, -1, 6]); 
axis([-3 3 -5 5]);

# make axes thick
h=get(gcf, "currentaxes");
set(h, "fontsize", 14, "linewidth", 5);