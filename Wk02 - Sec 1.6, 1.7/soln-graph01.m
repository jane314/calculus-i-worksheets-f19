f = @(t) t.^3;
x = -3:.05:3;
plot(x,f(x),'linewidth',4,'b');
hold on
plot(x,zeros(1,length(x)),'linewidth',1,'k')
hold on
plot([-3 3],[0 0],'linewidth',1,'k')
hold on
plot([0 0],[-30 30],'linewidth',1,'k')