

# the function
f = @(t) 1./t;
%g = @(t) t - 3^(-.5) + 3^(-1.5);
%h = @(t) t + 3^(-.5) - 3^(-1.5);
%m = (f(1.003)-f(1))/.003;
%g = @(t) m.*(t-1) + f(1);


vx = .5:.05:5.5;
%vy = 50*ones(1,length(vx));
%vx2 = 2.1:1:10;
%vx2 = -5:.1:5;
%dot = [1,3];







plot(vx, f(vx), 'k', 'linewidth', 3);
hold on

uno = ones(1,21);

for j = 1:4
    horiz = j:.05:j+1;
    %disp(horiz);
    vertL = 0:.05:f(j+1);
    plot(horiz, zeros(1,length(horiz)), 'b','linewidth',5);
    hold on
    plot(horiz, f(j+1).*ones(1,length(horiz)), 'b','linewidth',5);
    hold on
    plot(j.*ones(1,length(vertL)), vertL, 'b','linewidth',5);
    hold on
    plot((j+1).*ones(1,length(vertL)), vertL, 'b','linewidth',5);
    hold on
endfor
%plot(vx, g(vx), 'b', 'linewidth', 3);
%hold on
%plot(vx, h(vx), 'b', 'linewidth', 3);
%hold on
%scatter(dot,f(dot),100,'r','filled');
%hold on
%plot([-5 5], [0, 0], 'k', 'linewidth', 1);
%hold on 
%plot([0, 0], [-5 5],'k', 'linewidth', 1);

# make it a square
%axis ([0, 1, 0, 60]); 
%axis([-2 4 -40 40]);

# make axes thick
h=get(gcf, "currentaxes");
set(h, "fontsize", 14, "linewidth", 5);