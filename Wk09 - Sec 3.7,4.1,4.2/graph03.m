

# the function
f = @(t) t;
%g = @(t) t - 3^(-.5) + 3^(-1.5);
%h = @(t) t + 3^(-.5) - 3^(-1.5);
%m = (f(1.003)-f(1))/.003;
%g = @(t) m.*(t-1) + f(1);


vx = -3:.1:3;
%vy = 50*ones(1,length(vx));
%vx2 = 2.1:1:10;
%vx2 = -5:.1:5;
%dot = [1,3];







plot(vx, f(vx), 'k', 'linewidth', 3);
hold on
plot([-3 3],[0,0],'k','linewidth',1);
hold on
plot([0 0],[-3, 3],'k','linewidth',1);

%axis([-2 4 -40 40]);

# make axes thick
h=get(gcf, "currentaxes");
set(h, "fontsize", 14, "linewidth", 5);