f = @(t) 3+t;
g = @(t) (t.^2-2);
h = @(t) 10-t;

k = @(t) f(t) .* (-4 <= t && t <= -2) + g(t) .* (-2 < t && t <= 3) + h(t) .* (3 <t);

x1 = -4:.05:-2;
x2 = -1.95:.05:3;
x3 = 3:.05:5;


plot(x1,f(x1),'k','linewidth',4);
hold on
plot(x2,x2.^2-2,'k','linewidth',4);
hold on
plot(x3,h(x3),'k','linewidth',4);

##%hold off
##plot(x1,g(x1),'k','linewidth,',3)
