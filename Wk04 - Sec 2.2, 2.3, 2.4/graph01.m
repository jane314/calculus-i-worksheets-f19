

# the function
f = @(t) 0.5 .* (-t.^2+1) .* (t<1) + (t-t.^2) .* (t >= 1);


vx = -3:.1:5;
vx2 = -5:.1:5;
%dot = [1];


# make axes thick
h=get(gcf, "currentaxes");
set(h, "fontsize", 14, "linewidth", 5);




plot(vx2, f(vx2), 'k', 'linewidth', 5);
##hold on
##plot(vx2,g(vx2),'r','linewidth',3);
##
##hold on
##scatter([1],[f(1)],70,'b','filled');
##
##
##
### make it a square
##%axis ([0, 5, -1, 6]); 
##axis([-1 6 -40 40]);