

# the function
f = @(t) sin(t);
g = @(t) cos(t);

vx = 0:.05:pi/2;
s = [pi/4];







plot(vx, f(vx), 'k', 'linewidth', 5);
hold on
plot(vx,g(vx), 'b', 'linewidth', 5);
hold on
plot([-1 4],[0,0],'k','linewidth',1);
hold on
scatter(s,f(s),200,'r','filled');

# make it a square
axis ([0, pi/2, -1.5, 1.5]); 
%axis([-1.5 1.5 -6 6]);


# make axes thick
h=get(gcf, "currentaxes");
set(h, "fontsize", 14, "linewidth", 2);