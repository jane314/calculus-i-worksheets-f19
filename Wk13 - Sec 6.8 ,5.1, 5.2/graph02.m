

# the function
f = @(t) t.^(.5);
g = @(t) t.^(2);

vx = 0:.01:3;
s = [0,1];







plot(vx, f(vx), 'k', 'linewidth', 5);
hold on
plot(vx,g(vx), 'b', 'linewidth', 5);
hold on
plot([-1 4],[0,0],'k','linewidth',1);
hold on
scatter(s,f(s),200,'r','filled');

# make it a square
axis ([-.25, 1.25, -.5, 2]); 
%axis([-1.5 1.5 -6 6]);


# make axes thick
h=get(gcf, "currentaxes");
set(h, "fontsize", 14, "linewidth", 2);