

# the function
f = @(t) sin(t);
m = (f(pi + 0.003)-f(pi))/.003;
g = @(t) m.*(t-pi) + f(pi);


vx = 0:.1:5;
vx2 = -5:.1:5;
dot = [1];


# make axes thick
h=get(gcf, "currentaxes");
set(h, "fontsize", 14, "linewidth", 5);




plot(vx2, f(vx2), 'k', 'linewidth', 5);
hold on
plot(vx2,g(vx2),'r','linewidth',3);

hold on
scatter([pi],[f(pi)],70,'b','filled');



# make it a square
%axis ([0, 5, -1, 6]); 
axis([1 5 -2 2]);
