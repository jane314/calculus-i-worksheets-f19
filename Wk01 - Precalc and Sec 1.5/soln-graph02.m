f = @(t) (2.*x-3)./((x-2).*(x+4))
f = @(x) (2.*x-3)./((x-2).*(x+4))

x = -5:.05:3;
plot(x,f(x))
x1 = -5:.05:1.95;

x2 = 2.05:.05:3.05;
plot(x1,f(x1),'k','linewidth',4);
hold on
plot(x2,f(x2),'k','linewidth',4)
hold on
plot([2 2],[-40 40],'linewidth',2,'r')
plot([-4 -4],[-40 40],'linewidth',2,'r')