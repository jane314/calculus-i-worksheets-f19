

# the function
f = @(t) t.^2;

vx = -3:.05:20;
s = [1];





plot(vx, f(vx), 'k', 'linewidth', 5);
hold on
for i = 0:.1:2
	plot([i i], [0 i^2], 'b', 'linewidth', 5);
	hold on
endfor
plot([0 0],[-100,100],'r','linewidth',3);
hold on
plot([2 2],[-100,100],'r','linewidth',3);
hold on
plot([-100 100],[0 0],'k','linewidth',1);
hold on

# make it a square
axis ([-1.5, 3, -1, 5]); 
%axis([-1.5 1.5 -6 6]);


# make axes thick
h=get(gcf, "currentaxes");
set(h, "fontsize", 14, "linewidth", 2);